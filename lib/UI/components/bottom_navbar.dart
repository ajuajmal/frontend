import 'package:Sumedha/UI/common/themes/color_constants.dart';
import 'package:Sumedha/providers/bottom_navbar_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bottomBarProvider = Provider.of<BottomNavigationBarProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
          border: Border(top: BorderSide(color: Colors.black, width: 1.5))),
      child: BottomNavigationBar(
        elevation: 0,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        selectedItemColor: Theme.of(context).textTheme.subtitle1.color,
        showUnselectedLabels: true,
        unselectedItemColor:
            Theme.of(context).textTheme.subtitle1.color.withAlpha(90),
        currentIndex: bottomBarProvider.currentIndex,
        onTap: (index) {
          bottomBarProvider.currentIndex = index;
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assessment),
            title: Text('Test'),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            title: Text('Material'),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.date_range),
            title: Text(
              'Schedule',
            ),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          )
        ],
      ),
    );
  }
}
