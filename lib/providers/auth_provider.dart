import 'dart:convert';

import 'package:Sumedha/models/user.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/services/auth.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/foundation.dart';

class AuthService extends ChangeNotifier implements AuthBase {
  User user;
  String userData;
  String username, password;
  bool autoLogin = false,
      tokenFound = false,
      loginDataFound = false,
      loggedIn = false;
  final DataSource _dataSource = DataSource();
  final StorageHelper _storageHelper = StorageHelper();

  Future<User> loginUser(
      {@required String username, @required String password}) async {
    if (!loggedIn) {
      User tempUser;
      tempUser =
          await _dataSource.login(username: username, password: password);
      if (tempUser == null) return null;
      this.user = tempUser;
      await saveTokenDataToLocal();
      this.username = username;
      this.password = password;
      await saveLoginDataToLocal();
      notifyListeners();
      this.loggedIn = true;
      await _storageHelper.saveString(key: 'auto_login', value: 'true');
    }
    return user;
  }

  Future saveTokenDataToLocal() async {
    await _storageHelper.saveMapData(
        key: AuthStringConstants.userData, data: user.res);
    await _storageHelper.saveString(
        key: AuthStringConstants.access, value: user.access);
    await _storageHelper.saveString(
        key: AuthStringConstants.refresh, value: user.refresh);
  }

  Future<User> refreshUser() async {
    if (DateTime.now().isAfter(user.exp)) {
      User tempUser = await _dataSource.refresh(user.refresh);
      if (tempUser != null) {
        this.user = tempUser;
        await saveTokenDataToLocal();

        return tempUser;
      } else {
        this.user = null;

        return null;
      }
    }
    return this.user;
  }

  Future<User> getUserDataFromToken() async {
    this.userData =
        await _storageHelper.getString(AuthStringConstants.userData) ?? '';

    if (userData == '') {
      return null;
    }
    User tempUser = User.fromMap(jsonDecode(userData));
    this.user = tempUser;
    this.tokenFound = true;
    return tempUser;
  }

  Future<bool> checkIfLoginDataSaved() async {
    var username = await _storageHelper.getString(AuthStringConstants.username);
    var password = await _storageHelper.getString(AuthStringConstants.password);
    if (username == null || password == null) {
      this.loginDataFound = false;
      return false;
    }
    this.password = password;
    this.username = username;
    this.loginDataFound = true;
    return true;
  }

  Future<bool> checkIfUserDataSaved() async {
    final String s =
        await _storageHelper.storage.read(key: AuthStringConstants.userData);
    if (s == null) {
      this.tokenFound = false;
      return false;
    } else {
      this.tokenFound = true;
    }
    return true;
  }

  Future<User> speedLogin() async {
    bool userLoginDetailsFound = await checkIfLoginDataSaved();
    if (userLoginDetailsFound) {
      return await loginUser(username: username, password: password);
    } else
      return null;
  }

  @override
  Future<User> signInSilently() async {
    User tempUser;

    bool userDataFound = await checkIfUserDataSaved() ?? false;
    bool loginDataFound = await checkIfLoginDataSaved() ?? false;
    await checkAutoLogin();
    if (loginDataFound) {
      this.username =
          await _storageHelper.getString(AuthStringConstants.username);
      this.password =
          await _storageHelper.getString(AuthStringConstants.password);
    }
    if (userDataFound) {
      try {
        if (autoLogin) {
          tempUser = await loginUser(username: username, password: password);
          this.user = tempUser;
          notifyListeners();
          return user;
        } else
          tempUser = await getUserDataFromToken();
        if (tempUser == null) return null;
      } catch (e) {
        print(e);
      }

      if (user.exp.isAfter(DateTime.now())) {
        this.user = tempUser;
        notifyListeners();

        return tempUser;
      } else {
        //tempUser = await refreshUser();

        if (tempUser == null && loginDataFound) {
          tempUser = await loginUser(username: username, password: password);
          if (tempUser == null) return null;
          this.user = tempUser;

          notifyListeners();
          return user;
        }
        this.user = tempUser;

        notifyListeners();
        return tempUser;
      }
    } else if (loginDataFound) {
      tempUser = await loginUser(username: username, password: password);
      if (tempUser == null) return null;
      this.user = tempUser;

      notifyListeners();
      return tempUser;
    } else
      return null;
  }

  Future<void> signOut() async {
    this.user = null;
    await _storageHelper.storage.delete(key: AuthStringConstants.userData);
    await _storageHelper.storage.delete(key: AuthStringConstants.username);
    await _storageHelper.storage.delete(key: AuthStringConstants.password);
    await _storageHelper.saveString(key: 'auto_login', value: 'false');

    notifyListeners();
  }

  Future<void> saveLoginDataToLocal() async {
    await _storageHelper.saveString(
        key: AuthStringConstants.username, value: username);
    await _storageHelper.saveString(
        key: AuthStringConstants.password, value: password);
  }

  Future<void> checkAutoLogin() async {
    String s = await _storageHelper.getString('auto_login') ?? 'false';
    if (s == 'true') {
      this.autoLogin = true;
    } else {
      this.autoLogin = false;
    }
  }
}
