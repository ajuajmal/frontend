import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class NetworkUtil {
  static NetworkUtil _instance = NetworkUtil.internal();
  final http.Client _client = http.Client();

  NetworkUtil.internal();
  factory NetworkUtil() => _instance;
  final JsonDecoder _decoder = JsonDecoder();

  Future<dynamic> get({
    @required String url,
    Map headers = const <String, String>{},
  }) {
    return _client.get(url, headers: headers).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        print(response.statusCode);
        print(response.body);
        throw Exception("Error while fetching data");
      }
      return _decoder.convert(res);
    });
  }

  Future<dynamic> post(String url,
      {Map headers = const <String, String>{},
      body = const <String, String>{},
      encoding = const <String, String>{}}) {
    return _client
        .post(
      url,
      body: body,
      headers: headers,
    )
        .then((http.Response response) {
      final String res = response.body;

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        return null;
      }
      return _decoder.convert(res);
    });
  }

  Future<dynamic> read({
    @required String url,
    Map headers = const <String, String>{},
  }) async {
    try {
      String response = await _client.read(
        url,
        headers: headers,
      );

      return response;
    } on http.ClientException {
      return http.ClientException;
    } catch (e) {
      throw e;
    }
  }
}
