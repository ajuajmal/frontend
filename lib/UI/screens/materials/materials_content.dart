import 'package:Sumedha/UI/components/app_drawer.dart';
import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/screens/materials/materials_content_widget.dart';
import 'package:Sumedha/models/materials.dart';
import 'package:Sumedha/providers/subject_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MaterialsContent extends StatefulWidget {
  final int id;
  final String categoryName, subjectName;

  const MaterialsContent(
      {Key key,
      @required this.id,
      @required this.categoryName,
      @required this.subjectName})
      : super(key: key);
  @override
  _MaterialsContentState createState() => _MaterialsContentState();
}

class _MaterialsContentState extends State<MaterialsContent> {
  @override
  Widget build(BuildContext context) {
    final SubjectProvider subjectProvider =
        Provider.of<SubjectProvider>(context, listen: false);
    return Container(
      child: Scaffold(
        drawer: AppDrawer(),
        appBar: SumedhaAppBar(),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                widget.subjectName + ' ' + widget.categoryName,
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
            FutureBuilder<List<Materials>>(
                future: subjectProvider.getMaterials(widget.id),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.length == 0) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Center(
                          child: Text('No Materials found'),
                        ),
                      );
                    }

                    return Expanded(
                      child: ListView.builder(
                        itemCount: snapshot.data.length + 1,
                        itemBuilder: (context, int index) {
                          if (index == snapshot.data.length) {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              // child: Column(
                              //   children: [
                              //     Row(
                              //       mainAxisAlignment: MainAxisAlignment.center,
                              //       children: [
                              //         SizedBox(
                              //           height: 20,
                              //           width: 20,
                              //           child: CircularProgressIndicator(
                              //             valueColor:
                              //                 new AlwaysStoppedAnimation<Color>(
                              //                     Colors.green),
                              //           ),
                              //         ),
                              //         SizedBox(
                              //           width: 20,
                              //         ),
                              //         Text('Downloading'),
                              //       ],
                              //     ),
                              //     SizedBox(
                              //       height: 20,
                              //     ),
                              //     Row(
                              //       mainAxisAlignment: MainAxisAlignment.center,
                              //       children: [
                              //         Icon(
                              //           Icons.check,
                              //           color: Colors.green,
                              //         ),
                              //         SizedBox(
                              //           width: 20,
                              //         ),
                              //         Text('Ready To View'),
                              //       ],
                              //     ),
                              //     SizedBox(
                              //       height: 16,
                              //     ),
                              //   ],
                              // ),
                            );
                          }
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: MaterialsButton(
                              material: snapshot.data[index],
                            ),
                          );
                        },
                      ),
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
