// To parse this JSON data, do
//
//     final materialRead = materialReadFromMap(jsonString);

import 'dart:convert';

List<MaterialRead> materialReadFromMap(String str) => List<MaterialRead>.from(
    json.decode(str).map((x) => MaterialRead.fromMap(x)));

String materialReadToMap(List<MaterialRead> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class MaterialRead {
  MaterialRead({
    this.id,
    this.read,
    this.material,
    this.user,
  });

  int id;
  bool read;
  int material;
  int user;

  MaterialRead copyWith({
    int id,
    bool read,
    int material,
    int user,
  }) =>
      MaterialRead(
        id: id ?? this.id,
        read: read ?? this.read,
        material: material ?? this.material,
        user: user ?? this.user,
      );

  factory MaterialRead.fromMap(Map<String, dynamic> json) => MaterialRead(
        id: json["id"],
        read: json["read"],
        material: json["material"],
        user: json["user"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "read": read,
        "material": material,
        "user": user,
      };
}
