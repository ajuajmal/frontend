class LogoStringConstants {
  static const String sdsDark = 'assets/images/sds_dark.png';
  static const String sdsLight = 'assets/images/sds_light.png';
  static const String logo = 'assets/images/logo.png';
  static const String sdsLogoURL =
      'https://studevsoc.com/staticfiles/sdshome/img/logo.png';
  static const String logoURL =
      'https://www.sumedha.org.in/images/Sumedha_logo.png';
}

class ThemeStringConstants {
  static const String theme = 'theme';
  static const String light = 'light';
  static const String dark = 'dark';
}

class AuthStringConstants {
  static const String userData = 'user_data';
  static const String access = 'access';
  static const String refresh = 'refresh';
  static const String username = 'username';
  static const String password = 'password';
  static const String error = 'error';
  static const String errorMessage = 'error_msg';
  static const String accessTokenExpiry = 'access_token_expiry';
  static const String userID = 'user_id';
}
