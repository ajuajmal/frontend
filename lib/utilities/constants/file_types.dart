class FileTypes {
  static const Map<String, int> fileTypeMap = <String, int>{
    'none': 4,
    'video': 5,
    'audio': 15,
    'pdf': 20,
    'ppt': 21
  };
}

enum FileType { none, video, audio, pdf, ppt }
