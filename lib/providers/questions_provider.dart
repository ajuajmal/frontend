import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/cupertino.dart';

class QuestionsProvider extends ChangeNotifier {
  DataSource _dataSource = DataSource();
  Questions questions;
  int currentQuestion = 0;
  List<int> markedAnswers = [];
  StorageHelper _storageHelper = StorageHelper();
  Future<Questions> getAllQuestions({@required String slug}) async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);

    questions = await _dataSource.getQuestions(access: access, slug: slug);
    return questions;
  }

  void nextQuestion() {
    if (currentQuestion < questions.exam.questionSet.length - 1) {
      currentQuestion++;
      notifyListeners();
    }
  }

  void prevQuestion() {
    if (currentQuestion > 0) {
      currentQuestion--;
      notifyListeners();
    }
  }

  void selectAnswer(int option) {
    markedAnswers.insert(currentQuestion, option);
  }

  QuestionSet getQuestion(int questionNumber) {
    currentQuestion = questionNumber;
    return questions.exam.questionSet[questionNumber];
  }

  Future<void> getNewAccessToken() async {
    String refresh =
        await _storageHelper.getString(AuthStringConstants.refresh);
    await _dataSource.refresh(refresh).then((value) async => {
          await _storageHelper.saveString(
              key: AuthStringConstants.access, value: value.access)
        });
  }
}
