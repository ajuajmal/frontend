// To parse this JSON data, do
//
//     final questions = questionsFromMap(jsonString);

import 'dart:convert';

Questions questionsFromMap(String str) => Questions.fromMap(json.decode(str));

String questionsToMap(Questions data) => json.encode(data.toMap());

class Questions {
  Questions({
    this.exam,
    this.lastQuestionId,
  });

  Exam exam;
  dynamic lastQuestionId;

  Questions copyWith({
    Exam exam,
    dynamic lastQuestionId,
  }) =>
      Questions(
        exam: exam ?? this.exam,
        lastQuestionId: lastQuestionId ?? this.lastQuestionId,
      );

  factory Questions.fromMap(Map<String, dynamic> json) => Questions(
        exam: Exam.fromMap(json["exam"]),
        lastQuestionId: json["last_question_id"],
      );

  Map<String, dynamic> toMap() => {
        "exam": exam.toMap(),
        "last_question_id": lastQuestionId,
      };
}

class Exam {
  Exam({
    this.id,
    this.examtakersSet,
    this.questionSet,
    this.name,
    this.description,
    this.file,
    this.slug,
    this.rollOut,
    this.timestamp,
    this.subject,
  });

  int id;
  ExamtakersSet examtakersSet;
  List<QuestionSet> questionSet;
  String name;
  String description;
  String file;
  String slug;
  bool rollOut;
  DateTime timestamp;
  int subject;

  Exam copyWith({
    int id,
    ExamtakersSet examtakersSet,
    List<QuestionSet> questionSet,
    String name,
    String description,
    String file,
    String slug,
    bool rollOut,
    DateTime timestamp,
    int subject,
  }) =>
      Exam(
        id: id ?? this.id,
        examtakersSet: examtakersSet ?? this.examtakersSet,
        questionSet: questionSet ?? this.questionSet,
        name: name ?? this.name,
        description: description ?? this.description,
        file: file ?? this.file,
        slug: slug ?? this.slug,
        rollOut: rollOut ?? this.rollOut,
        timestamp: timestamp ?? this.timestamp,
        subject: subject ?? this.subject,
      );

  factory Exam.fromMap(Map<String, dynamic> json) => Exam(
        id: json["id"],
        examtakersSet: ExamtakersSet.fromMap(json["examtakers_set"]),
        questionSet: List<QuestionSet>.from(
            json["question_set"].map((x) => QuestionSet.fromMap(x))),
        name: json["name"],
        description: json["description"],
        file: json["file"],
        slug: json["slug"],
        rollOut: json["roll_out"],
        timestamp: DateTime.parse(json["timestamp"]),
        subject: json["subject"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "examtakers_set": examtakersSet.toMap(),
        "question_set": List<dynamic>.from(questionSet.map((x) => x.toMap())),
        "name": name,
        "description": description,
        "file": file,
        "slug": slug,
        "roll_out": rollOut,
        "timestamp": timestamp.toIso8601String(),
        "subject": subject,
      };
}

class ExamtakersSet {
  ExamtakersSet({
    this.id,
    this.studentsanswerSet,
    this.score,
    this.completed,
    this.dateFinished,
    this.timestamp,
    this.user,
    this.exam,
  });

  int id;
  List<StudentsanswerSet> studentsanswerSet;
  int score;
  bool completed;
  dynamic dateFinished;
  DateTime timestamp;
  int user;
  int exam;

  ExamtakersSet copyWith({
    int id,
    List<StudentsanswerSet> studentsanswerSet,
    int score,
    bool completed,
    dynamic dateFinished,
    DateTime timestamp,
    int user,
    int exam,
  }) =>
      ExamtakersSet(
        id: id ?? this.id,
        studentsanswerSet: studentsanswerSet ?? this.studentsanswerSet,
        score: score ?? this.score,
        completed: completed ?? this.completed,
        dateFinished: dateFinished ?? this.dateFinished,
        timestamp: timestamp ?? this.timestamp,
        user: user ?? this.user,
        exam: exam ?? this.exam,
      );

  factory ExamtakersSet.fromMap(Map<String, dynamic> json) => ExamtakersSet(
        id: json["id"],
        studentsanswerSet: List<StudentsanswerSet>.from(
            json["studentsanswer_set"]
                .map((x) => StudentsanswerSet.fromMap(x))),
        score: json["score"],
        completed: json["completed"],
        dateFinished: json["date_finished"],
        timestamp: DateTime.parse(json["timestamp"]),
        user: json["user"],
        exam: json["exam"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "studentsanswer_set":
            List<dynamic>.from(studentsanswerSet.map((x) => x.toMap())),
        "score": score,
        "completed": completed,
        "date_finished": dateFinished,
        "timestamp": timestamp.toIso8601String(),
        "user": user,
        "exam": exam,
      };
}

class StudentsanswerSet {
  StudentsanswerSet({
    this.id,
    this.examTaker,
    this.question,
    this.answer,
  });

  int id;
  int examTaker;
  int question;
  dynamic answer;

  StudentsanswerSet copyWith({
    int id,
    int examTaker,
    int question,
    dynamic answer,
  }) =>
      StudentsanswerSet(
        id: id ?? this.id,
        examTaker: examTaker ?? this.examTaker,
        question: question ?? this.question,
        answer: answer ?? this.answer,
      );

  factory StudentsanswerSet.fromMap(Map<String, dynamic> json) =>
      StudentsanswerSet(
        id: json["id"],
        examTaker: json["exam_taker"],
        question: json["question"],
        answer: json["answer"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "exam_taker": examTaker,
        "question": question,
        "answer": answer,
      };
}

class QuestionSet {
  QuestionSet({
    this.id,
    this.answerSet,
    this.label,
    this.order,
    this.exam,
  });

  int id;
  List<AnswerSet> answerSet;
  String label;
  int order;
  int exam;

  QuestionSet copyWith({
    int id,
    List<AnswerSet> answerSet,
    String label,
    int order,
    int exam,
  }) =>
      QuestionSet(
        id: id ?? this.id,
        answerSet: answerSet ?? this.answerSet,
        label: label ?? this.label,
        order: order ?? this.order,
        exam: exam ?? this.exam,
      );

  factory QuestionSet.fromMap(Map<String, dynamic> json) => QuestionSet(
        id: json["id"],
        answerSet: List<AnswerSet>.from(
            json["answer_set"].map((x) => AnswerSet.fromMap(x))),
        label: json["label"],
        order: json["order"],
        exam: json["exam"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "answer_set": List<dynamic>.from(answerSet.map((x) => x.toMap())),
        "label": label,
        "order": order,
        "exam": exam,
      };
}

class AnswerSet {
  AnswerSet({
    this.id,
    this.question,
    this.label,
  });

  int id;
  int question;
  String label;

  AnswerSet copyWith({
    int id,
    int question,
    String label,
  }) =>
      AnswerSet(
        id: id ?? this.id,
        question: question ?? this.question,
        label: label ?? this.label,
      );

  factory AnswerSet.fromMap(Map<String, dynamic> json) => AnswerSet(
        id: json["id"],
        question: json["question"],
        label: json["label"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "question": question,
        "label": label,
      };
}
