import 'package:Sumedha/UI/screens/home/home_screen.dart';
import 'package:Sumedha/UI/screens/splash/splash_widget.dart';
import 'package:Sumedha/UI/screens/splash/welcome_screen.dart';
import 'package:Sumedha/providers/auth_provider.dart';
import 'package:Sumedha/providers/theme_changer.dart';
import 'package:Sumedha/services/auth.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  final AuthBase auth;

  const SplashScreen({Key key, this.auth}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    ScreenSize screenSize = ScreenSize(context);
    final theme = Provider.of<Themer>(context);
    return Consumer<AuthService>(
      builder: (context, auth, child) => FutureBuilder(
          future: auth.signInSilently(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              auth.user = snapshot.data;
              return HomePage();
            } else {
              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasError) {
                return Scaffold(body: WelcomeScreen());
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Scaffold(
                  body: SplashWidget(
                    theme: theme,
                    screenSize: screenSize,
                  ),
                );
              }
              return Scaffold();
            }
          }),
    );
  }

  Future<Map<String, String>> getSavedLoginDetails() async {
    final StorageHelper _storageHelper = StorageHelper();
    String u = await _storageHelper.getString(AuthStringConstants.username);
    String p = await _storageHelper.getString(AuthStringConstants.password);
    if (u.isNotEmpty && p.isNotEmpty) {
      return <String, String>{
        AuthStringConstants.username: u,
        AuthStringConstants.password: p,
      };
    }
    return <String, String>{
      AuthStringConstants.username: '',
      AuthStringConstants.password: '',
    };
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
