import 'dart:math';

import 'package:Sumedha/UI/common/loader.dart';
import 'package:Sumedha/UI/common/themes/base_theme.dart';
import 'package:Sumedha/UI/common/themes/input_decoration_theme.dart';
import 'package:Sumedha/UI/screens/home/home_screen.dart';
import 'package:Sumedha/models/user.dart';
import 'package:Sumedha/providers/auth_provider.dart';
import 'package:Sumedha/providers/theme_changer.dart';
import 'package:Sumedha/utilities/constants/enums.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  final String username, password;

  const LoginScreen({Key key, this.username, this.password}) : super(key: key);
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String userName, pwd;
  Map<String, String> loginDetails;
  bool isLoading = false;
  final _loginFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    ScreenSize screenSize = ScreenSize(context);
    final theme = Provider.of<Themer>(context);

    InputDecorationTheme inputTheme = theme.appTheme == AppTheme.Dark
        ? InputDecorationThemes.loginInputDecorationThemeDark
        : InputDecorationThemes.loginInputDecorationThemeLight;

    return Consumer<AuthService>(
      builder: (context, auth, child) => FutureBuilder(
          future: auth.signInSilently(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Scaffold(
                body: Container(
                  child: CustomPaint(
                    painter: OvalPrinter(),
                    child: SingleChildScrollView(
                        child: Form(
                      key: _loginFormKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 70),
                            child: SizedBox(
                              height: screenSize.height / 5,
                              child: Center(
                                child: GestureDetector(
                                  onTap: () {
                                    theme.setTheme(BaseTheme.lightTheme);
                                  },
                                  onDoubleTap: () {
                                    theme.setTheme(BaseTheme.darkTheme);
                                  },
                                  child: Image.asset(
                                    LogoStringConstants.logo,
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(),
                                padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
                                child: Text(
                                  'Welcome To Sumedha Academy ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      letterSpacing: 1.25,
                                      height: 1.4,
                                      fontSize: 28,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(40, 30, 40, 0),
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  child: FutureBuilder(
                                      future: StorageHelper().getString(
                                          AuthStringConstants.username),
                                      builder: (context, snapshot) {
                                        return TextFormField(
                                          initialValue: snapshot.hasData
                                              ? snapshot.data
                                              : '',
                                          onChanged: (v) {
                                            userName = v;
                                          },
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Please enter username';
                                            }

                                            return null;
                                          },
                                          textAlign: TextAlign.center,
                                          decoration: InputDecoration()
                                              .applyDefaults(inputTheme)
                                              .copyWith(
                                                hintText: 'User Name',
                                              ),
                                        );
                                      }),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(40, 8, 40, 0),
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  child: FutureBuilder(
                                      future: StorageHelper().getString(
                                          AuthStringConstants.password),
                                      builder: (context, snapshot) {
                                        return TextFormField(
                                          onChanged: (v) {
                                            pwd = v;
                                          },
                                          initialValue: snapshot.hasData
                                              ? snapshot.data
                                              : '',
                                          obscureText: true,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Please enter password';
                                            }
                                            if (value.length < 1) {
                                              return 'Minimum 8 characters';
                                            }
                                            return null;
                                          },
                                          textAlign: TextAlign.center,
                                          decoration: InputDecoration()
                                              .applyDefaults(inputTheme)
                                              .copyWith(
                                                hintText: 'Password',
                                              ),
                                        );
                                      }),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(60, 20, 60, 16),
                                child: SizedBox(
                                  height: 50,
                                  child: RaisedButton(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 50, vertical: 10),
                                    color: Colors.black,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    ),
                                    child: Text(
                                      'Login',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 14),
                                    ),
                                    onPressed: () async {
                                      User _user;
                                      setState(() {
                                        isLoading = true;
                                      });
                                      if (_loginFormKey.currentState
                                          .validate()) {
                                        _user = await auth
                                            .loginUser(
                                          username: userName,
                                          password: pwd,
                                        )
                                            .whenComplete(() {
                                          isLoading = false;
                                        });
                                        if (_user == null) {
                                          setState(() {
                                            isLoading = false;
                                          });

                                          Scaffold.of(context).showSnackBar(
                                              SnackBar(
                                                  content: Text(
                                                      'User not found for the entered credentials')));
                                        }
                                      } else {
                                        setState(() {
                                          isLoading = false;
                                        });
                                        Scaffold.of(context).showSnackBar(SnackBar(
                                            content: Text(
                                                'Please check login details')));
                                      }
                                    },
                                  ),
                                ),
                              ),
                              isLoading ? LinearProgressIndicator() : SizedBox()
                            ],
                          ),
                        ],
                      ),
                    )),
                  ),
                ),
              );
            } else if (snapshot.hasData)
              return HomePage();
            else
              return Scaffold(
                body: Center(
                  child: Loader(),
                ),
              );
          }),
    );
  }
}

class OvalPrinter extends CustomPainter {
  Paint _paint;

  OvalPrinter() {
    _paint = Paint()
      ..color = Colors.green
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 10;
  }

  @override
  void paint(Canvas canvas, Size size) {
    _paint.color = Colors.redAccent;
    _paint.style = PaintingStyle.fill;
    var circleRect = Offset(150, 450) & Size(400, 450);
    canvas.drawArc(circleRect, -pi / 3, pi * 3, false, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
