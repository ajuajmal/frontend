import 'package:Sumedha/UI/common/themes/color_constants.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class QuestionCard extends StatefulWidget {
  const QuestionCard({
    Key key,
    @required this.question,
    @required this.options,
    @required this.questionNumber,
  }) : super(key: key);

  final String question;
  final List<String> options;
  final int questionNumber;

  @override
  _QuestionCardState createState() => _QuestionCardState();
}

class _QuestionCardState extends State<QuestionCard> {
  int selectedAnswer = 0;

  @override
  Widget build(BuildContext context) {
    final ScreenSize screenSize = ScreenSize(context);
    final questionProvider = Provider.of<QuestionsProvider>(context);

    return Column(
      children: [
        Card(
          elevation: 1,
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.black, width: 1.5),
              borderRadius: BorderRadius.circular(40)),
          margin: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: Container(
            padding: EdgeInsets.all(4),
            child: Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Text(
                          widget.questionNumber.toString() + ') ',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Text(
                            widget.question,
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                        ),
                      ],
                    ),
                  ),
                  RadioListTile(
                    activeColor: ColorConstants.primaryRed,
                    groupValue: selectedAnswer,
                    onChanged: (v) {
                      setState(() {
                        selectedAnswer = v;
                        questionProvider.selectAnswer(selectedAnswer);
                      });
                    },
                    value: 0,
                    title: Text(widget.options[0]),
                  ),
                  RadioListTile(
                    activeColor: ColorConstants.primaryRed,
                    groupValue: selectedAnswer,
                    onChanged: (v) {
                      setState(() {
                        selectedAnswer = v;
                        questionProvider.selectAnswer(selectedAnswer);
                      });
                    },
                    value: 1,
                    title: Text(widget.options[1]),
                  ),
                  RadioListTile(
                    activeColor: ColorConstants.primaryRed,
                    groupValue: selectedAnswer,
                    onChanged: (v) {
                      setState(() {
                        setState(() {
                          selectedAnswer = v;
                          questionProvider.selectAnswer(selectedAnswer);
                        });
                      });
                    },
                    value: 2,
                    title: Text(widget.options[2]),
                  ),
                  RadioListTile(
                    activeColor: ColorConstants.primaryRed,
                    groupValue: selectedAnswer,
                    onChanged: (v) {
                      setState(() {
                        selectedAnswer = v;
                        questionProvider.selectAnswer(selectedAnswer);
                      });
                    },
                    value: 3,
                    title: Text(widget.options[3]),
                  ),
                ],
              ),
            ),
            constraints: BoxConstraints(
                minHeight: screenSize.height / 2, minWidth: screenSize.width),
          ),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                questionProvider.prevQuestion();
              },
              child: FaIcon(
                FontAwesomeIcons.backward,
                size: 35,
              ),
            ),
            SizedBox(
              width: 56,
            ),
            GestureDetector(
              onTap: () {
                questionProvider.nextQuestion();
              },
              child: FaIcon(
                FontAwesomeIcons.forward,
                size: 35,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
