import 'package:flutter/material.dart';

// Light Theme Login Decoration

class InputDecorationThemes {
  static InputDecorationTheme loginInputDecorationThemeLight =
      ThemeData.light().inputDecorationTheme.copyWith(
            fillColor: Colors.white,
            filled: true,
            hintStyle: TextStyle(
                color: Colors.black, fontSize: 19, fontWeight: FontWeight.w300),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.grey,
                width: 2,
              ),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.red,
                width: 1,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.red,
                width: 2,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.black,
                width: 2,
              ),
            ),
          );

// Dark Theme Login Decoration

  static InputDecorationTheme loginInputDecorationThemeDark =
      ThemeData.dark().inputDecorationTheme.copyWith(
            filled: true,
            fillColor: Colors.black,
            hintStyle: TextStyle(
                color: Color(0xff595869),
                fontSize: 19,
                fontWeight: FontWeight.bold),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.grey,
                width: 3,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.white,
                width: 2,
              ),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.red,
                width: 1,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                color: Colors.red,
                width: 2,
              ),
            ),
          );
}
