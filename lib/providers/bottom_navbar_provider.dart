import 'package:Sumedha/UI/screens/coming_soon/coming_soon_page.dart';
import 'package:Sumedha/UI/screens/dashboard/dashboard.dart';
import 'package:Sumedha/UI/screens/materials/materials_page.dart';
import 'package:flutter/material.dart';

class BottomNavigationBarProvider with ChangeNotifier {
  // final List tabItems = [
  //   DashBoardScreen(),
  //   TestsPage(),
  //   MaterialsPage(),
  //   SchedulePage()
  // ];

  final List tabItems = [
    DashBoardScreen(),
    ComingSoonPage(),
    MaterialsPage(),
    ComingSoonPage(),
  ];
  int _currentIndex = 0;

  get currentIndex => _currentIndex;

  set currentIndex(int index) {
    _currentIndex = index;

    notifyListeners();
  }
}
