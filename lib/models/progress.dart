// To parse this JSON data, do
//
//     final progressModel = progressModelFromMap(jsonString);

import 'dart:convert';

ProgressModel progressModelFromMap(String str) =>
    ProgressModel.fromMap(json.decode(str));

String progressModelToMap(ProgressModel data) => json.encode(data.toMap());

class ProgressModel {
  ProgressModel({
    this.materialCount,
    this.userReadCount,
    this.percentage,
  });

  int materialCount;
  int userReadCount;
  double percentage;

  ProgressModel copyWith({
    int materialCount,
    int userReadCount,
    double percentage,
  }) =>
      ProgressModel(
        materialCount: materialCount ?? this.materialCount,
        userReadCount: userReadCount ?? this.userReadCount,
        percentage: percentage ?? this.percentage,
      );

  factory ProgressModel.fromMap(Map<String, dynamic> json) {
    return ProgressModel(
      materialCount: json["material_count"],
      userReadCount: json["user_read_count"],
      percentage: json["percentage"],
    );
  }

  Map<String, dynamic> toMap() => {
        "material_count": materialCount,
        "user_read_count": userReadCount,
        "percentage": percentage,
      };
}
