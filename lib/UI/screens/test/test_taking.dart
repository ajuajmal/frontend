import 'package:Sumedha/UI/components/app_drawer.dart';
import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/screens/test/question_card_widget.dart';
import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class TestTakingScreen extends StatefulWidget {
  final String subjectName, testName, slug;
  final Duration testDuration;
  final int totalQuestions;
  const TestTakingScreen(
      {Key key,
      @required this.subjectName,
      @required this.testName,
      @required this.testDuration,
      @required this.totalQuestions,
      @required this.slug})
      : super(key: key);
  @override
  _TestTakingScreenState createState() => _TestTakingScreenState();
}

class _TestTakingScreenState extends State<TestTakingScreen> {
  @override
  Widget build(BuildContext context) {
    final ScreenSize screenSize = ScreenSize(context);
    final questionProvider = Provider.of<QuestionsProvider>(context);
    return Scaffold(
      drawer: AppDrawer(),
      appBar: SumedhaAppBar(),
      body: FutureBuilder<Questions>(
          future: questionProvider.getAllQuestions(slug: widget.slug),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(
                child: CircularProgressIndicator(),
              );
            return SingleChildScrollView(
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 32,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16),
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.pop(context),
                            child: Container(
                              padding:
                                  const EdgeInsets.only(left: 24, right: 32),
                              child: FaIcon(
                                FontAwesomeIcons.caretLeft,
                                size: 35,
                              ),
                            ),
                          ),
                          Flexible(
                            child: Text(
                              snapshot.data.exam.name,
                              softWrap: false,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.fade,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Text(snapshot.data.exam.description,
                        style: Theme.of(context).textTheme.subtitle1),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16.0, 20, 16, 2),
                      child: Text(
                        '14:49 minutes left',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            .copyWith(color: Colors.red),
                      ),
                    ),
                    Text(
                        (widget.totalQuestions -
                                    questionProvider.currentQuestion)
                                .toString() +
                            ' Questions left',
                        style: Theme.of(context).textTheme.bodyText2),
                    QuestionCard(
                      questionNumber: questionProvider.currentQuestion,
                      question: questionProvider.questions.exam
                          .questionSet[questionProvider.currentQuestion].label,
                      options: [
                        questionProvider
                            .questions
                            .exam
                            .questionSet[questionProvider.currentQuestion]
                            .answerSet[0]
                            .label,
                        questionProvider
                            .questions
                            .exam
                            .questionSet[questionProvider.currentQuestion]
                            .answerSet[1]
                            .label,
                        questionProvider
                            .questions
                            .exam
                            .questionSet[questionProvider.currentQuestion]
                            .answerSet[2]
                            .label,
                        questionProvider
                            .questions
                            .exam
                            .questionSet[questionProvider.currentQuestion]
                            .answerSet[3]
                            .label
                      ],
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
