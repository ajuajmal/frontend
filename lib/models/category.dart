// To parse this JSON data, do
//
//     final category = categoryFromMap(jsonString);

import 'dart:convert';

List<Category> categoryFromMap(String str) =>
    List<Category>.from(json.decode(str).map((x) => Category.fromMap(x)));

String categoryToMap(List<Category> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Category {
  Category({
    this.id,
    this.name,
    this.category,
    this.subject,
  });

  int id;
  String name;
  int category;
  int subject;

  Category copyWith({
    int id,
    String name,
    int category,
    int subject,
  }) =>
      Category(
        id: id ?? this.id,
        name: name ?? this.name,
        category: category ?? this.category,
        subject: subject ?? this.subject,
      );

  factory Category.fromMap(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        category: json["category"],
        subject: json["subject"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "category": category,
        "subject": subject,
      };
}
