import 'package:Sumedha/UI/components/app_drawer.dart';
import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/components/bottom_navbar.dart';
import 'package:Sumedha/models/user.dart';
import 'package:Sumedha/providers/auth_provider.dart';
import 'package:Sumedha/providers/bottom_navbar_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthService>(context);
    return Consumer<BottomNavigationBarProvider>(
      builder: (context, bottomBarProvider, _) => FutureBuilder<User>(
          future: auth.refreshUser(),
          builder: (context, snapshot) {
            if (snapshot.hasData)
              return Scaffold(
                drawer: AppDrawer(),
                appBar: SumedhaAppBar(),
                body:
                    bottomBarProvider.tabItems[bottomBarProvider.currentIndex],
                bottomNavigationBar: BottomNavBar(),
              );
            return Scaffold(body: Center(child: CircularProgressIndicator()));
          }),
    );
  }
}
