FROM ubuntu:18.04 as base
   
# Prerequisites
RUN apt update && apt install -y curl git unzip xz-utils zip libglu1-mesa openjdk-8-jdk wget
RUN apt-get -y install curl gawk g++ gcc make libc6-dev libreadline6-dev \
zlib1g-dev libssl-dev libyaml-dev autoconf \
libgdbm-dev libncurses5-dev automake libtool bison pkg-config libffi-dev
RUN apt-get install gnupg2 -y
RUN curl -sSL https://rvm.io/pkuczynski.asc | gpg2 --import -
RUN curl -L https://get.rvm.io | bash -s stable
RUN apt install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm5 libgdbm-dev
ENV PATH /usr/local/rvm/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
RUN rvm install ruby-2.7.0
RUN rvm reload
CMD source /etc/profile
CMD source /usr/local/rvm/scripts/rvm   

RUN ["/bin/bash", "-l", "-c", "rvm use 2.7.0 --default" ]
RUN ["/bin/bash", "-l", "-c", "rvm requirements; gem install bundler --no-document"]
RUN ["/bin/bash", "-l", "-c", "gem list"]
RUN apt-get update 
RUN apt-get install rubygems
RUN gem install bundler

# Set up new user
RUN useradd -ms /bin/bash developer
USER developer
WORKDIR /home/developer

# Prepare Android directories and system variables
RUN mkdir -p Android/sdk
ENV ANDROID_SDK_ROOT /home/developer/Android/sdk
RUN mkdir -p .android && touch .android/repositories.cfg
   
# Set up Android SDK
RUN wget -O sdk-tools.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
RUN unzip sdk-tools.zip && rm sdk-tools.zip
RUN mv tools Android/sdk/tools
RUN cd Android/sdk/tools/bin && yes | ./sdkmanager --licenses
RUN cd Android/sdk/tools/bin && ./sdkmanager "build-tools;29.0.2" "patcher;v4" "platform-tools" "platforms;android-29" "sources;android-29"
ENV PATH "$PATH:/home/developer/Android/sdk/platform-tools"

# Download Flutter SDK
RUN git clone https://github.com/flutter/flutter.git
ENV PATH "$PATH:/home/developer/flutter/bin"
   
# Run basic check to download Dark SDK
RUN flutter doctor
