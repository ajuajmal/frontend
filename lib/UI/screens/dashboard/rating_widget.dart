import 'package:Sumedha/UI/common/themes/color_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

typedef void RatingChangeCallback(double rating);

class StarRating extends StatelessWidget {
  final int starCount;
  final double rating;
  final RatingChangeCallback onRatingChanged;
  final Color color;

  StarRating(
      {this.starCount = 5, this.rating = .0, this.onRatingChanged, this.color});

  Widget buildStar(BuildContext context, int index) {
    Widget icon;
    if (index >= rating) {
      icon = Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0),
        child: CircleAvatar(
          radius: 10,
          backgroundColor: ColorConstants.primaryRed,
          child: CircleAvatar(
            radius: 9,
            backgroundColor: Theme.of(context).cardColor,
          ),
        ),
      );
    } else if (index > rating - 1 && index < rating) {
      icon = Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0),
        child: SvgPicture.asset(
          'assets/svg/half-circle.svg',
          height: 20,
          width: 20,
        ),
      );
    } else {
      icon = Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0),
        child: CircleAvatar(
          radius: 10,
          backgroundColor: ColorConstants.primaryRed,
        ),
      );
    }
    return new InkResponse(
//      onTap: onRatingChanged == null ? null : () => onRatingChanged(index + 1.0),
      child: icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Row(
        children:
            new List.generate(starCount, (index) => buildStar(context, index)));
  }
}

class Rating extends StatefulWidget {
  final double rating;

  const Rating({Key key, @required this.rating}) : super(key: key);
  @override
  _RatingState createState() => new _RatingState();
}

class _RatingState extends State<Rating> {
  double rating;
  @override
  void initState() {
    rating = widget.rating;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StarRating(
      rating: rating,
      onRatingChanged: (rating) => setState(() => this.rating = rating),
    );
  }
}
