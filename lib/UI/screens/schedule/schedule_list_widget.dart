import 'package:Sumedha/UI/screens/schedule/schedule_cards_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ScheduleListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ScheduleCard(
            title: 'Subject 1',
            subtitle: 'Tomorrow 11:00 AM - 11:30 AM',
            icon: FontAwesomeIcons.flask,
            color: Colors.blueAccent,
          ),
          ScheduleCard(
            title: 'Subject 2',
            subtitle: 'Tomorrow 10:00 AM - 11:00 AM',
            icon: FontAwesomeIcons.calculator,
            color: Colors.blueAccent,
          ),
          ScheduleCard(
            title: 'Subject 3',
            subtitle: 'Tomorrow 12:30 PM - 1:30 PM',
            icon: FontAwesomeIcons.flask,
            color: Colors.blueAccent,
          ),
          ScheduleCard(
            title: 'Subject 4',
            subtitle: 'Tomorrow 3:00 PM - 3:30 AM',
            icon: FontAwesomeIcons.flask,
            color: Colors.blueAccent,
          ),
        ],
      ),
    );
  }
}
