import 'package:flutter_driver/driver_extension.dart';
import 'package:Sumedha/main.dart' as app;
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
void main() {
  var driver;
  // This line enables the extension.
  enableFlutterDriverExtension();
  // Call the `main()` function of the app, or call `runApp` with
  // any widget you are interested in testing.
  app.main();
  final writeDataFinder = find.byValueKey("write_data");
  final addDataFinder = find.byValueKey("add_data");

  setUpAll(() async {
     driver = await FlutterDriver.connect();
  });

  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });
}

