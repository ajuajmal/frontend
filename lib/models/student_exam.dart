// To parse this JSON data, do
//
//     final studentExam = studentExamFromMap(jsonString);

import 'dart:convert';

List<StudentExam> studentExamFromMap(String str) =>
    List<StudentExam>.from(json.decode(str).map((x) => StudentExam.fromMap(x)));

String studentExamToMap(List<StudentExam> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class StudentExam {
  StudentExam({
    this.id,
    this.name,
    this.description,
    this.file,
    this.slug,
    this.questionsCount,
    this.completed,
    this.score,
    this.progress,
  });

  int id;
  String name;
  String description;
  String file;
  String slug;
  int questionsCount;
  bool completed;
  dynamic score;
  int progress;

  StudentExam copyWith({
    int id,
    String name,
    String description,
    String file,
    String slug,
    int questionsCount,
    bool completed,
    dynamic score,
    int progress,
  }) =>
      StudentExam(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        file: file ?? this.file,
        slug: slug ?? this.slug,
        questionsCount: questionsCount ?? this.questionsCount,
        completed: completed ?? this.completed,
        score: score ?? this.score,
        progress: progress ?? this.progress,
      );

  factory StudentExam.fromMap(Map<String, dynamic> json) => StudentExam(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        file: json["file"],
        slug: json["slug"],
        questionsCount: json["questions_count"],
        completed: json["completed"],
        score: json["score"],
        progress: json["progress"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "description": description,
        "file": file,
        "slug": slug,
        "questions_count": questionsCount,
        "completed": completed,
        "score": score,
        "progress": progress,
      };
}
