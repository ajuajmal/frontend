import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class IconConstants {
  static Map<String, IconData> iconMapping = {
    'chemistry': FontAwesomeIcons.flask,
    'physics': FontAwesomeIcons.atom,
    'maths': FontAwesomeIcons.ruler,
    'science': FontAwesomeIcons.calculator,
    'gk': FontAwesomeIcons.globeAsia
  };
}
