import 'package:Sumedha/UI/screens/materials/materials_content.dart';
import 'package:Sumedha/models/category.dart';
import 'package:Sumedha/models/subject.dart';
import 'package:Sumedha/providers/subject_provider.dart';
import 'package:Sumedha/utilities/constants/icon_constants.dart';
import 'package:Sumedha/utilities/constants/utility_fuctions.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class MaterialsCard extends StatelessWidget {
  final Subject subject;
  MaterialsCard({@required this.subject});
  @override
  Widget build(BuildContext context) {
    final SubjectProvider subjectProvider =
        Provider.of<SubjectProvider>(context, listen: false);
    final ScreenSize screenSize = ScreenSize(context);
    return Card(
      shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.black, width: 1.5),
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      elevation: 0,
      margin: EdgeInsets.all(12.0),
      child: ExpansionTile(
        title: _buildTitle(context),
        trailing: Icon(Icons.list),
        children: <Widget>[
          Divider(
            indent: 8,
            endIndent: 8,
          ),
          Container(
            margin: EdgeInsets.only(
                top: 8,
                bottom: 24,
                left: screenSize.width / 8,
                right: screenSize.width / 8),
            child: FutureBuilder<List<Category>>(
                future: subjectProvider.getCategory(subject.id),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      shrinkWrap: true,
                      itemBuilder: (context, int index) {
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 12),
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            elevation: 3,
                            onPressed: () async {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => MaterialsContent(
                                            id: snapshot.data[index].id,
                                            categoryName:
                                                snapshot.data[index].name,
                                            subjectName: subject.name,
                                          )));
                            },
                            child: Text(snapshot.data[index].name),
                            color: Theme.of(context).colorScheme.background,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: Theme.of(context)
                                        .textTheme
                                        .headline1
                                        .color),
                                borderRadius: BorderRadius.circular(30)),
                          ),
                        );
                      },
                      itemCount: snapshot.data.length,
                    );
                  }

                  return CircularProgressIndicator();
                }),
          )
        ],
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    final ScreenSize screenSize = ScreenSize(context);
    final Color color = UtilityFunction.parseColor(subject.color);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 24, left: 8),
              child: FaIcon(
                  IconConstants.iconMapping[subject.name.toLowerCase()] ??
                      FontAwesomeIcons.book,
                  size: 35,
                  color: color),
            ),
            Expanded(
              child: Container(
                height: screenSize.height / 7.5,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        subject.name ?? '',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: Text(
                        subject.description ?? '',
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
