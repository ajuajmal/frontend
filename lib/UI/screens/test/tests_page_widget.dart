import 'package:Sumedha/UI/screens/test/test_taking.dart';
import 'package:Sumedha/models/exams.dart';
import 'package:Sumedha/models/student_exam.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TestCard extends StatelessWidget {
  final Exam exam;
  TestCard({@required this.exam});
  @override
  Widget build(BuildContext context) {
    final ScreenSize screenSize = ScreenSize(context);
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => TestTakingScreen(
                    slug: exam.slug,
                    totalQuestions: exam.questionsCount,
                    testDuration: Duration(minutes: 14, seconds: 44),
                    subjectName: exam.name,
                    testName: exam.description,
                  ))),
      child: Card(
        margin: EdgeInsets.fromLTRB(20, 16, 20, 16),
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.black, width: 1.5),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 24, right: 24),
              child: FaIcon(FontAwesomeIcons.book, size: 35, color: Colors.red),
            ),
            Expanded(
              flex: 2,
              child: Container(
                height: screenSize.height / 6,
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        exam.name,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: Text(
                        exam.description,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: Text(
                        'Total questions : ' + exam.questionsCount.toString(),
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class StudentExamCard extends StatelessWidget {
  final StudentExam exam;
  StudentExamCard({@required this.exam});
  @override
  Widget build(BuildContext context) {
    final ScreenSize screenSize = ScreenSize(context);
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => TestTakingScreen(
                    slug: exam.slug,
                    totalQuestions: exam.questionsCount,
                    testDuration: Duration(minutes: 14, seconds: 44),
                    subjectName: exam.name,
                    testName: exam.description,
                  ))),
      child: Card(
        margin: EdgeInsets.fromLTRB(20, 16, 20, 16),
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.black, width: 1.5),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 24, right: 24),
              child: FaIcon(FontAwesomeIcons.book, size: 35, color: Colors.red),
            ),
            Expanded(
              flex: 2,
              child: Container(
                height: screenSize.height / 6,
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        exam.name,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: Text(
                        exam.description,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: Text(
                        'Total questions : ' + exam.questionsCount.toString(),
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
