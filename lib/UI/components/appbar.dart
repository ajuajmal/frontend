import 'package:Sumedha/UI/components/search_bar.dart';
import 'package:flutter/material.dart';

class SumedhaAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;

  SumedhaAppBar() : preferredSize = Size.fromHeight(56.0);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        child: AppBar(
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Sumedha',
            style: Theme.of(context).textTheme.headline5,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(
                  context: context,
                  delegate: CustomSearchBar(),
                );
              },
            ),
          ],
        ));
  }
}
